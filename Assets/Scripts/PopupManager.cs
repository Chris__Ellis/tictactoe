﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;

public class PopupManager : MonoBehaviourPunCallbacks
{
    public GameObject sizePopup;

    public GameObject winPopup;
    public TextMeshProUGUI winText;

    public GameObject drawPopup;

    public GameObject connectionPopup;
    public TextMeshProUGUI connectionText;

    public GameObject startPopup;

    public GameObject waitPopup;
    
    public override void OnEnable()
    {
        base.OnEnable();
        GameState.showWinner += ShowWin;
        GameState.showDraw += ShowDraw;
        GameState.drawBoard += StartGame;
        GameState.placePiece += WaitForTurn;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        GameState.showWinner -= ShowWin;
        GameState.showDraw -= ShowDraw;
        GameState.drawBoard -= StartGame;
    }

    /// <summary>
    /// Open the popup asking the user for board size.
    /// </summary>
    void ShowSize()
    {
        sizePopup.SetActive(true);
    }

    /// <summary>
    /// Dismiss the popup asking the user for the board size.
    /// This happens when we draw the board.
    /// </summary>
    /// <param name="size">Used to subscribe to the event.</param>
    void StartGame(byte size)
    {
        sizePopup.SetActive(false);
        //If we're online we also have to hide the connection panel.
        connectionPopup.SetActive(false);
        //If we're online and aren't the first player, start waiting.
        if (PhotonNetwork.IsConnected && !PhotonNetwork.IsMasterClient)
        {
            waitPopup.SetActive(true);
        }
    }

    /// <summary>
    /// Show the popup with the winner of the game.
    /// </summary>
    /// <param name="winner"></param>
    void ShowWin(bool winner)
    {
        waitPopup.SetActive(false);
        //X is true, O is false.
        winText.text = winner ? "X" : "O";
        winPopup.SetActive(true);
    }

    /// <summary>
    /// Show the popup calling a draw.
    /// </summary>
    void ShowDraw()
    {
        waitPopup.SetActive(false);
        drawPopup.SetActive(true);
    }

    #region Online
    void WaitForTurn(bool player, byte x, byte y)
    {
        //If we arne't connected, we're playing locally, no waiting.
        if (PhotonNetwork.IsConnected)
        {
            waitPopup.SetActive(!waitPopup.activeInHierarchy);
        }
    }

    public override void OnConnectedToMaster()
    {
        connectionText.text = "Connected to master server...";
    }

    public override void OnJoinedLobby()
    {
        connectionText.text = "Connected to lobby, finding room...";
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        connectionText.text = "No games found, connecting to a new room...";
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        connectionText.text = "Connecting...";
        //The client called disconnect, so don't treat it as an error.
        if (cause == DisconnectCause.DisconnectByClientLogic)
        {
            return;
        }
        //Hide any in game messages, because the game may have stopped unexpectedly.
        startPopup.SetActive(false);
        winPopup.SetActive(false);
        drawPopup.SetActive(false);
        waitPopup.SetActive(false);
        connectionText.text = "You have been disconnected.\nWait a moment and try again.";
        connectionPopup.SetActive(true);
        StartCoroutine(IEReset());
    }

    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            connectionText.text = "Waiting for another player...";
        }
        else
        {
            connectionText.text = "Joined a room.";
            StartCoroutine(IEStartConnection(true));
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        connectionText.text = "A player has joined.";
        StartCoroutine(IEStartConnection(true));
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        connectionText.text = "The other player has left.\nYou win!";
        StartCoroutine(IEReset());
    }

    //How long we leave disconnect popups on the screen.
    const float WAIT_TIME = 3.0f;

    IEnumerator IEStartConnection(bool wait)
    {
        StartCoroutine(IESetWaitSize());
        if (wait)
        {
            yield return new WaitForSeconds(WAIT_TIME);
        }
        if (PhotonNetwork.IsMasterClient)
        {
            connectionPopup.SetActive(false);
        }
    }

    IEnumerator IESetWaitSize()
    {
        yield return new WaitForSeconds(WAIT_TIME);
        if (PhotonNetwork.IsMasterClient)
        {
            sizePopup.SetActive(true);
        }
        else
        {
            connectionText.text = "Waiting for host to choose board size.";
            connectionPopup.SetActive(true);
        }
    }

    IEnumerator IEReset()
    {
        yield return new WaitForSeconds(WAIT_TIME);
        connectionPopup.SetActive(false);
        connectionText.text = "Connecting...";
        startPopup.SetActive(true);
    }
    
    #endregion
}