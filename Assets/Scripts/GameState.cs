﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class GameState : MonoBehaviourPunCallbacks
{
    public enum State { Blank, X, O };

    public delegate void DrawBoard(byte size);
    public static event DrawBoard drawBoard;

    public delegate void PlacePiece(bool player, byte x, byte y);
    public static event PlacePiece placePiece;

    public delegate void ShowWinner(bool winner);
    public static event ShowWinner showWinner;

    public delegate void ShowDraw();
    public static event ShowDraw showDraw;

    bool gameStarted = false;

    //Size of the board
    byte size = 3;
    State[][] board;
    byte moveCount = 0;

    //Who's turn is it?
    State state = State.X;

    //Online variables.
    bool isOnline = false;
    State localState = State.X;
    byte version = 1;

    //Must be public to match the base OnEnable.
    public override void OnEnable()
    {
        base.OnEnable();
        Board.registerClick += Move;
    }

    //Must be public to match the base OnDisable.
    public override void OnDisable()
    {
        base.OnDisable();
        Board.registerClick -= Move;
    }

    /// <summary>
    /// Sets the size and initializes the board.
    /// </summary>
    /// <param name="size">The number of columns and rows of the board.</param>
    public void SetSize(string size_text)
    {

        //Our input field passes a string, so we need to try to convert to a byte.
        byte size;
        if (!byte.TryParse(size_text, out size))
        {
            return;
        }
        if (size < 2)
        {
            return;
        }

        SetSize(size);
    }

    /// <summary>
    /// Seperated to use as an RPC sending only a byte instead of a string.
    /// </summary>
    /// <param name="size">The size of the board.</param>
    [PunRPC]
    void SetSize(byte size)
    { 
        //When we're online tell the other player to set the board.
        if (isOnline && PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("SetSize", RpcTarget.Others, size);
        }

        this.size = size;

        //Initializes the board.
        ResetBoard();

        //Fire event to draw board.
        drawBoard?.Invoke(size);
        gameStarted = true;
    }

    /// <summary>
    /// An RPC used to call move for the other player's turn.
    /// </summary>
    /// <param name="x">The row of the move.</param>
    /// <param name="y">The column of the move.</param>
    [PunRPC]
    void RPCMove(byte x, byte y)
    {
        Move(x, y, true);
    }

    /// <summary>
    /// Check to see if a move is valid, if it is it fires the event to place the piece and checks for a winner.
    /// </summary>
    /// <param name="x">The row of the move.</param>
    /// <param name="y">The column of the move.</param>
    void Move(byte x, byte y, bool otherTurn)
    {
        if(isOnline && state != localState && !otherTurn)
        {
            return;
        }
        //We can only place a piece where there isn't already a piece.
        if (gameStarted && board[x][y] == State.Blank)
        {
            board[x][y] = state;
            //X = true, O = false.
            placePiece(state == State.X ? true : false, x, y);
        }
        else
            return;
        
        if(isOnline)
        {
            photonView.RPC("RPCMove", RpcTarget.Others, x, y);
        }
        moveCount++;

        #region ConditionChecks

        //Check column.
        for (byte i = 0; i < size; i++)
        {
            if (board[x][i] != state)
                break;
            if (i == size - 1)
            {
                ReportWin(state);
            }
        }

        //Check row.
        for (byte i = 0; i < size; i++)
        {
            if (board[i][y] != state)
                break;
            if (i == size - 1)
            {
                ReportWin(state);
            }
        }

        //Check forward diagonal.
        if (x == y)
        {
            for (byte i = 0; i < size; i++)
            {
                if (board[i][i] != state)
                    break;
                if (i == size - 1)
                {
                    ReportWin(state);
                }
            }
        }

        //Check for backwards diagonal.
        if (x + y == size - 1)
        {
            for (byte i = 0; i < size; i++)
            {
                if (board[i][(size - 1) - i] != state)
                    break;
                if (i == size - 1)
                {
                    ReportWin(state);
                }
            }
        }

        //Check for a draw.
        if (moveCount == Mathf.Pow(size, 2))
        {
            //State.Blank means there's a draw.
            ReportWin(State.Blank);
        }
        #endregion

        //Advance to the next player.
        if(state == State.X)
        {
            state = State.O;
        }
        else
        {
            state = State.X;
        }
    }

    /// <summary>
    /// Overload for events.
    /// </summary>
    /// <param name="x">Row of the move.</param>
    /// <param name="y">Column of the move.</param>
    void Move(byte x, byte y)
    {
        Move(x, y, false);
    }

    /// <summary>
    /// Fire off the winner event for the right player.
    /// </summary>
    /// <param name="winner">The winning player.</param>
    void ReportWin(State winner)
    {
        gameStarted = false;
        //Clean up.
        ResetBoard();
        switch(winner)
        {
            case State.X:
                showWinner?.Invoke(true);
                break;
            case State.O:
                showWinner?.Invoke(false);
                break;
            case State.Blank:
                showDraw?.Invoke();
                break;
        }

        StartCoroutine(WaitAndDisconnect());  
    }
    
    /// <summary>
    /// Clear the data board and get ready for a new game.
    /// </summary>
    void ResetBoard()
    {
        //X always goes first.
        state = State.X;
        moveCount = 0;

        board = new State[size][];
        for (byte i = 0; i < size; i++)
        {
            board[i] = new State[size];
        }

    }

    #region Online
    /// <summary>
    /// Called when you choose an onlinle game from the menu to connect to Photon.
    /// </summary>
    public void Connect()
    {
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.GameVersion = version + "." + SceneManagerHelper.ActiveSceneBuildIndex;
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 2 }, null);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        ResetBoard();
        isOnline = false;
    }

    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            localState = State.X;    
        }
        else
        {
            localState = State.O;
        }
        isOnline = true;
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        ResetBoard();
        isOnline = false;
        PhotonNetwork.Disconnect();
    }

    IEnumerator WaitAndDisconnect()
    {
        if (isOnline)
        {
            yield return new WaitForSeconds(3);
            isOnline = false;
            PhotonNetwork.Disconnect();
        }
    }
    #endregion
}
