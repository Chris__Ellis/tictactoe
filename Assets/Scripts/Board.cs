﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vectrosity;
using Photon.Pun;
using Photon.Realtime;

public class Board : MonoBehaviourPunCallbacks
{
    public delegate void RegisterClick(byte x, byte y);
    public static event RegisterClick registerClick;

    public Transform pieceHolder;
    public GameObject xPrefab;
    public GameObject oPrefab;
    List<GameObject> pieces = new List<GameObject>();

    VectorLine line;
    List<Vector2> points = new List<Vector2>();
    byte width = 5;
    float boardSize = 0;
    //The number of pixels up from the bottom of the screen that we start drawing.
    float bottomPadding = 50.0f;
    float leftPadding = 0;
    //How many rows/columns the board has.
    byte size = 0;
    float cellSize = 0;

    public void Awake()
    {
        //Discrete lines do not connect, and require two points per segment.
        line = new VectorLine("board", points, width, LineType.Discrete);
        line.color = Color.black;

        //Dynamically adjust board size for all landscape and portrait screens.
        if (Screen.width > Screen.height)
        {
            boardSize = Screen.height - bottomPadding * 2;
            leftPadding = (Screen.width - boardSize) * 0.5f;
        }
        else
        {
            leftPadding = bottomPadding;
            boardSize = Screen.width - bottomPadding * 2;
            bottomPadding = (Screen.height - boardSize) * 0.5f;

        }
    }

    public override void OnEnable()
    {
        base.OnEnable();
        GameState.drawBoard += DrawBoard;
        GameState.placePiece += PlacePiece;
        //Clear the board after the game, no matter if it's a win or draw.
        GameState.showDraw += ClearBoard;
        GameState.showWinner += ClearBoard;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        GameState.drawBoard -= DrawBoard;
        GameState.placePiece -= PlacePiece;
        GameState.showDraw -= ClearBoard;
        GameState.showWinner -= ClearBoard;
    }

    void DrawBoard(byte size)
    {
        //Clear the line in case there's already a board drawn.
        points.Clear();

        this.size = size;
        
        cellSize = boardSize / size;

        for (int i = 1; i < size; i++)
        {
            //Vertical lines.
            points.Add(new Vector2(i * cellSize + leftPadding, boardSize + bottomPadding));
            points.Add(new Vector2(i * cellSize + leftPadding, bottomPadding));
            //Horizontal lines.
            points.Add(new Vector2(leftPadding, i * cellSize + bottomPadding));
            points.Add(new Vector2(leftPadding + boardSize, i * cellSize + bottomPadding));
        }

        line.Draw();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CheckForCell(Input.mousePosition);
        }
    }

    void CheckForCell(Vector3 pos)
    {
        //The board hasn't been initialized yet.
        if (size == 0)
            return;

        //Check for out of range.
        if(pos.x < leftPadding || pos.x > leftPadding + boardSize || pos.y > bottomPadding + boardSize || pos.y < bottomPadding)
        {
            return;
        }

        float cellX = (pos.x - leftPadding) / cellSize;
        float cellY = size - (pos.y - bottomPadding) / cellSize;
        
        registerClick?.Invoke((byte)cellX, (byte)cellY);
    }

    /// <summary>
    /// Overload of ClearBoard(bool) for events.
    /// </summary>
    void ClearBoard()
    {
        ClearBoard(true);
    }

    /// <summary>
    /// Erases all markers, empties the marker list, erases the board.
    /// </summary>
    /// <param name="x">Only used to register events.</param>
    void ClearBoard(bool x)
    {
        line.points2.Clear();
        line.Draw();
        foreach (GameObject go in pieces)
        {
            Destroy(go);
        }
        pieces.Clear();
    }

    /// <summary>
    /// Place the piece at the desired spot on the board.
    /// </summary>
    /// <param name="player">Who's turn is it?</param>
    /// <param name="cellX">The row of the play.</param>
    /// <param name="cellY">The column of the play.</param>
    void PlacePiece(bool player, byte cellX, byte cellY)
    {
        GameObject piece = Instantiate(player ? xPrefab : oPrefab, pieceHolder, false);
        //We store all the pieces to delete them when we clear the board.
        pieces.Add(piece);

        Vector2 pos = new Vector2();
        //UI elements count from the center of screen, so we need to offset.
        //The offset will be different for odd and even number boards.
        if (size % 2 == 0)
        {
            pos.x = cellSize * (cellX + 0.5f) - boardSize * 0.5f;
            pos.y = -cellSize * (cellY - 0.5f - (size * 0.5f - 1));
        }
        else
        {
            pos.x = cellSize * Mathf.FloorToInt(cellX - (size * 0.5f - 1));
            pos.y = -cellSize * Mathf.FloorToInt(cellY - (size * 0.5f - 1));
        }

        RectTransform rect = piece.GetComponent<RectTransform>();
        rect.localPosition = pos;

        //Resize the icon to match the size of the cell.
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, cellSize);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, cellSize);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        ClearBoard();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        ClearBoard();
    }
}
